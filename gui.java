import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

class gui {

    // This constructs the application window, then calls setup to fill in the details
    public void start(){
        JFrame frame = new JFrame("File Reader");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300,150);
        JPanel panel = new JPanel();
        frame.add(panel);
        setup(panel);
        frame.setVisible(true);
    }

    // This places the user prompt, text box, and submit button
    private void setup(JPanel panel){
        panel.setLayout(null);

        // This label prompts the user for a filename
        JLabel file = new JLabel("Enter filename here:");
        file.setBounds(10,20,150,25);
        panel.add(file);

        // This is the text field where the user enters a filename
        JTextField filename = new JTextField(20);
        filename.setBounds(130,20,120,25);
        panel.add(filename);

        // This is the "submit" button
        JButton submit = new JButton("Submit");
        submit.setBounds(80,60,80,25);

        // When the submit button is pushed, the user's entry is sent to the file manager
        submit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String file = filename.getText();
                try {
                    ArrayList<String> words = fileManager.loadFile(file);
                    fileManager.calculate(words);
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        });
        panel.add(submit);
    }
}