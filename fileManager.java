import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

class fileManager{

    public static ArrayList<String> loadFile(String filename) throws Exception{
        ArrayList<String> words = new ArrayList<>();
        Scanner s = new Scanner(new File(filename));
        while(s.hasNext()) {
            words.add(s.next());
        }
        s.close();

        // Making sure all words are in lower-case
        ArrayList<String> wordsToLowerCase = new ArrayList<>();
        for (String i : words){
            wordsToLowerCase.add(i.toLowerCase());
        }

        return wordsToLowerCase;
    }

    //Calculates the total number of words in the text file
    public static void calculate(ArrayList<String> words){
        int total = words.size();
        System.out.println("The total number of words is: " + total);
    }

}